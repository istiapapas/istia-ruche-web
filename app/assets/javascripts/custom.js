/**
 * Created by Thomas on 12/11/2014.
 */
Highcharts.setOptions({
    lang: {
        months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        shortMonths: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
        decimalPoint: ',',
        printChart: 'Imprimer',
        downloadPNG: 'Télécharger en image PNG',
        downloadJPEG: 'Télécharger en image JPEG',
        downloadPDF: 'Télécharger en document PDF',
        downloadSVG: 'Télécharger en document Vectoriel',
        loading: 'Chargement en cours...',
        contextButtonTitle: 'Exporter le graphique',
        resetZoom: 'Réinitialiser le zoom',
        resetZoomTitle: 'Réinitialiser le zoom au niveau 1:1',
        thousandsSep: ' ',
        decimalPoint: ',',
        noData: 'Pas d\'information à afficher'
    }
});

jQuery(function($) { $.extend({
    form: function(url, data, method) {
        if (method == null) method = 'POST';
        if (data == null) data = {};

        var form = $('<form>').attr({
            method: method,
            action: url
        }).css({
            display: 'none'
        });

        var addData = function(name, data) {
            if ($.isArray(data)) {
                for (var i = 0; i < data.length; i++) {
                    var value = data[i];
                    addData(name + '[]', value);
                }
            } else if (typeof data === 'object') {
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {
                        addData(name + '[' + key + ']', data[key]);
                    }
                }
            } else if (data != null) {
                form.append($('<input>').attr({
                    type: 'hidden',
                    name: String(name),
                    value: String(data)
                }));
            }
        };

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                addData(key, data[key]);
            }
        }

        return form.appendTo('body');
    }
}); });


$.fn.eqHeights = function(options) {

    var defaults = {
        child: false ,
        parentSelector:null
    };
    var options = $.extend(defaults, options);

    var el = $(this);
    if (el.length > 0 && !el.data('eqHeights')) {
        $(window).bind('resize.eqHeights', function() {
            el.eqHeights();
        });
        el.data('eqHeights', true);
    }

    if( options.child && options.child.length > 0 ){
        var elmtns = $(options.child, this);
    } else {
        var elmtns = $(this).children();
    }

    var prevTop = 0;
    var max_height = 0;
    var elements = [];
    var parentEl;
    elmtns.height('auto').each(function() {

        if(options.parentSelector && parentEl !== $(this).parents(options.parentSelector).get(0)){
            $(elements).height(max_height);
            max_height = 0;
            prevTop = 0;
            elements=[];
            parentEl = $(this).parents(options.parentSelector).get(0);
        }

        var thisTop = this.offsetTop;

        if (prevTop > 0 && prevTop != thisTop) {
            $(elements).height(max_height);
            max_height = $(this).height();
            elements = [];
        }
        max_height = Math.max(max_height, $(this).height());

        prevTop = this.offsetTop;
        elements.push(this);
    });

    $(elements).height(max_height);
};

//Ready function
var ready = function() {
    $('[class*="eq-"]').eqHeights();
    $('.tree-toggler').click(function () {
        $(this).parent().children('ul.tree').slideToggle(300);
    });

    <!-- Menu Toggle Script -->
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        setTimeout(function(){$('[class*="eq-"]').eqHeights();},400);
    });
};
$(document).ready(ready);
$(document).on('page:load', ready);
setTimeout(function(){$('[class*="eq-"]').eqHeights();},1000);

