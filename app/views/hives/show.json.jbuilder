json.extract! @hive, :id, :name, :created_at, :updated_at
sensorsJSON = []
@hive.arduinos.each do |arduino|
  arduino.sensors.each do |sensor|
    sensorsJSON.push(sensor)
  end
end
json.sensors sensorsJSON
