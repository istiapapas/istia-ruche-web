json.hives(@hives) do |hive|
  json.extract! hive, :id, :name
  json.url hive_url(hive, format: :json)
  sensorsJSON = []
  hive.arduinos.each do |arduino|
    arduino.sensors.each do |sensor|
      sensorsJSON.push(sensor)
    end
  end
  json.sensors sensorsJSON
end
