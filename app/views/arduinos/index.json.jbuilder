json.array!(@arduinos) do |arduino|
  json.extract! arduino, :id, :name, :hive_id, :serial
  json.url arduino_url(arduino, format: :json)
end
