json.array!(@data) do |datum|
  json.extract! datum, :id, :value, :sensor_id, :date
  json.url datum_url(datum, format: :json)
end
