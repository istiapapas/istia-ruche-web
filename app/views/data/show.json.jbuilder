json.extract! @datum, :id, :value, :sensor_id, :created_at, :updated_at
json.unit @datum.sensor.sensor_type.unit
