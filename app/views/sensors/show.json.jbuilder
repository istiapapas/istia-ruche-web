json.extract! @sensor, :id, :name, :created_at, :updated_at, :sensor_type
if !@sensor.arduino.nil? and !@sensor.arduino.hive.nil?
  json.hive_id @sensor.arduino.hive
else
  json.hive_id :null
end
json.data @data
