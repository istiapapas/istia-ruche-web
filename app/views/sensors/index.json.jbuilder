json.sensors(@sensors) do |sensor|
  json.extract! sensor, :id, :name, :sensor_type_id
  json.url sensor_url(sensor, format: :json)
  if !sensor.arduino.nil? and !sensor.arduino.hive.nil?
    json.hive_id sensor.arduino.hive
  else
    json.hive_id :null
  end
end
