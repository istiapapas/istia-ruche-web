class SensorType < ActiveRecord::Base
	has_many :sensors
	validates :name, :unit, presence: true
	validates :name, uniqueness: true
	mount_uploader :image, ImagesUploader
end
