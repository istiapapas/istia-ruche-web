class Arduino < ActiveRecord::Base
  has_many :sensors
  belongs_to :hive
  validates :name, :serial,  presence: true
  validates_uniqueness_of :name, scope: :hive
  validates_uniqueness_of :serial, scope: :hive
end
