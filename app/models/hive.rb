class Hive < ActiveRecord::Base
	has_many :arduinos
	validates :name,  presence: true
	validates :name, uniqueness: true
end
