class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable


  #ROLES
  ROLES = %w[banned registered moderator admin]
  #admin hérite de moderator qui lui même hérite de registered etc
  def role?(base_role)
    ROLES.index(base_role.to_s) <= ROLES.index(role)
  end

  def isAdmin
    return self.role == 'admin';
  end
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :set_default_role

  private
  def set_default_role
    if User.all.count < 1
      self.role ||= 'admin'
    else
      self.role ||= 'registered'
    end
  end
end
