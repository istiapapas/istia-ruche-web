class Datum < ActiveRecord::Base
  belongs_to :sensor
  validates :sensor_id, :value,  presence: true
  validates_uniqueness_of :created_at, scope: :sensor
end
