class Sensor < ActiveRecord::Base
	belongs_to :arduino
	belongs_to :sensor_type
	has_many :datum
	validates :name, :serial,  presence: true
  validates_uniqueness_of :serial, scope: :arduino
end