# encoding: utf-8
class HivesController < ApplicationController
  load_and_authorize_resource
  before_action :set_hive, only: [:show, :edit, :update, :destroy]

  # GET /hives
  # GET /hives.json
  def index
    @hives = Hive.all
    # State of the hives
    @hiveState = {}

    # For each hive
    @hives.each do |hive|
      # Create a list of all sensors
      sensors = []
      # For each arduino of the hive
      hive.arduinos.each do |arduino|
        # For each sensor of the arduino
        arduino.sensors.each do |sensor|
          # Add it to the list
          sensors.push(sensor)
        end
      end

      # Default state
      updatedRecently = true if sensors.size > 0

      # Retrieve time
      @tRef = Time.now
      # For each sensor
      sensors.each do |sensor|
        # If there are no date or the data haven't been updated since one hour
        if sensor.datum.size == 0 || (@tRef - sensor.datum.order('created_at ASC').all.last.created_at) > 25.hours
          updatedRecently = false
          break
        end

      end
      # Push it to the associative array
      @hiveState[hive.id] = updatedRecently
    end
  end

  def index2
    @hives = Hive.all
  end

  # GET /hives/1
  # GET /hives/1.json
  def show
    @hive
    # State of the sensor
    @sensorState = {}

    # Create a list of all sensors
    sensors = []
    # For each arduino of the hive
    @hive.arduinos.each do |arduino|
      # For each sensor of the arduino
      arduino.sensors.each do |sensor|
        # Add it to the list
        sensors.push(sensor)
      end
    end


    # Retrieve time
    @tRef = Time.now
    # For each sensor
    sensors.each do |sensor|
      # Default state
      updatedRecently = true
      # If there are no date or the data haven't been updated since one hour
      if sensor.datum.size == 0 || (@tRef - sensor.datum.order('created_at ASC').all.last.created_at) > 25.hours
        updatedRecently = false
      end

      # Push it to the associative array
      @sensorState[sensor.id] = updatedRecently
    end
  end

  # GET /hives/new
  def new
    @hive = Hive.new
  end

  # GET /hives/1/edit
  def edit
  end

  # POST /hives
  # POST /hives.json
  def create
    @hive = Hive.new(hive_params)

    respond_to do |format|
      if @hive.save
        format.html { redirect_to :back, notice: 'Ruche créée avec succès.' }
        format.json { render :show, status: :created, location: @hive }
      else
        format.html { render :new }
        format.json { render json: @hive.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hives/1
  # PATCH/PUT /hives/1.json
  def update
    respond_to do |format|
      if @hive.update(hive_params)
        format.html { redirect_to :back, notice: 'Ruche mise à jour avec succès.' }
        format.json { render :show, status: :ok, location: @hive }
      else
        format.html { render :edit }
        format.json { render json: @hive.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hives/1
  # DELETE /hives/1.json
  def destroy
    @hive.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Ruche supprimée avec succès.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hive
      if(!params[:hive_name].nil?)
        @hive = Hive.where(name: params[:hive_name]).first
        if(@hive.blank?)
            redirect_to action: "index"
        end
      else
        @hive = Hive.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hive_params
      params.require(:hive).permit(:name)
    end
end
