# encoding: utf-8
class ArduinosController < ApplicationController
  load_and_authorize_resource
  before_action :set_arduinos, only: [:show, :edit, :update, :destroy]

  # GET /arduinos
  # GET /arduinos.json
  def index
    @arduino = Arduino.all
  end

  # GET /arduino/1
  # GET /arduino/1.json
  def show
  end

  # GET /arduinos/new
  def new
    @arduinos = Arduino.new
  end

  # GET /arduinos/1/edit
  def edit
  end

  # POST /arduinos
  # POST /arduinos.json
  def create
    @arduinos = Arduino.new(arduino_params)

    respond_to do |format|
      if @arduinos.save
        format.html { redirect_to :back, notice: 'Arduino créé avec succès.' }
        format.json { render :show, status: :created, location: @arduinos }
      else
        format.html { render :new }
        format.json { render json: @arduinos.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /arduinos/1
  # PATCH/PUT /arduinos/1.json
  def update
    respond_to do |format|
      if @arduinos.update(arduino_params)
        format.html { redirect_to :back, notice: 'Arduino mise à jour avec succès.' }
        format.json { render :show, status: :ok, location: @arduinos }
      else
        format.html { render :edit }
        format.json { render json: @arduinos.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /arduinos/1
  # DELETE /arduinos/1.json
  def destroy
    @arduinos.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Arduino suppirmée avec succès.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_arduinos
    @arduinos = Arduino.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def arduino_params
    params.require(:arduino).permit(:name, :hive_id, :serial)
  end
end