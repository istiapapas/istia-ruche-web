# encoding: utf-8
class RegistrationsController < Devise::RegistrationsController
  def new
    flash[:alert] = 'Les inscriptions son désactivées'
    redirect_to root_path
  end

  def create
    flash[:alert] = 'Les inscriptions son désactivées'
    redirect_to root_path
  end
end 