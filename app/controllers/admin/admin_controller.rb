class Admin::AdminController < ApplicationController
  authorize_resource :class => false
  def index
    # Used as homepage for the administration of the website.
  end
end
