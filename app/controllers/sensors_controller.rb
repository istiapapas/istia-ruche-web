# encoding: utf-8
class SensorsController < ApplicationController
  load_and_authorize_resource
  before_action :set_sensor, only: [:show, :edit, :update, :destroy]

  # GET /sensors
  # GET /sensors.json
  def index
    @sensors = Sensor.all
  end

  # GET /sensors_admin
  # GET /sensors_admin.json
  def index2
    @sensors = Sensor.all
  end

  # GET /sensors/1
  # GET /sensors/1.json
  def show
    @point = []
    if !params[:limit].nil? && params[:limit].to_i > 0
      @data = @sensor.datum.limit(params[:limit]).order('created_at DESC').all.reverse
      @data.each do |d|
        t = d.created_at.to_i * 1000
        @point.push([t, d.value.to_i])
      end
    else
      @data = @sensor.datum.all.order('created_at DESC').all.reverse
      @data.each do |d|
        t = d.created_at.to_i * 1000
        @point.push([t, d.value.to_i])
      end
    end
    @chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.title :text => @sensor.name
      f.series :name=>@sensor.sensor_type.name, :data=>@point, :color => '#ebbc14'
      f.xAxis :type => 'datetime', :maxZoom => 14 * 24 * 3600000 # fourteen days
      f.chart :zoomType => 'x'
      f.tooltip :valueSuffix => @sensor.sensor_type.unit
      f.yAxis  :title => {:text => @sensor.sensor_type.name + " (" + @sensor.sensor_type.unit + ")"}
      f.rangeSelector :enabled => 'true', :buttons => [{type:'week',count:1,text:'1s'},{type:'month',count:1,text:'1m'},{type:'month',count:6,text:'6m'},{type:'year',count:1,text:'1an'},{type:'all',text:'Tout'}],
      :buttonSpacing => 10, :inputDateFormat => '%e %b %Y', :inputEditDateFormat => '%d-%m-%Y'
      f.scrollbar :enabled => 'true', :minWidth => 30
    end
  end

  # GET /sensors/compare
  def compare
    sensorsList = params[:sensors]
    if !sensorsList.nil? and sensorsList.size > 0
      yAxis = []
      i = 0
      series = []
      sensorsList.each do |sensorId|
        point = []
        sensor = Sensor.find_by_id(sensorId)
          data = sensor.datum.all.order('created_at DESC').all.reverse
          data.each do |d|
            t = d.created_at.to_i * 1000
            point.push([t, d.value.to_i])
          end
        yAxis.push({:title => {:text => sensor.sensor_type.name + " (" + sensor.sensor_type.unit + ")"}})
        series.push({:name => sensor.name + ' (' + sensor.sensor_type.name + ')', :yAxis => i, :data => point, :tooltip => {:valueSuffix => sensor.sensor_type.unit}})
        i = i+1
      end

        @chart = LazyHighCharts::HighChart.new('graph') do |f|
          f.title :text => 'Comparaison'
          f.xAxis :type => 'datetime', :maxZoom => 14 * 24 * 3600000 # fourteen days
          f.chart :zoomType => 'x'
          series.each do |s|
            f.series(s)
          end
          f.yAxis yAxis
          f.rangeSelector :enabled => 'true', :buttons => [{type:'week',count:1,text:'1s'},{type:'month',count:1,text:'1m'},{type:'month',count:6,text:'6m'},{type:'year',count:1,text:'1an'},{type:'all',text:'Tout'}],
                          :buttonSpacing => 10, :inputDateFormat => '%e %b %Y', :inputEditDateFormat => '%d-%m-%Y'
          f.scrollbar :enabled => 'true', :minWidth => 30
        end
    else
      render Rails.application.routes.recognize_path(request.referer)[:action]
    end
  end

  # GET /sensors/new
  def new
    @sensor = Sensor.new
  end

  # GET /sensors/1/edit
  def edit
  end

  # POST /sensors
  # POST /sensors.json
  def create
    @sensor = Sensor.new(sensor_params)

    respond_to do |format|
      if @sensor.save
        format.html { redirect_to :back, notice: 'Capteur créé avec succès.' }
        format.json { render :show, status: :created, location: @sensor }
      else
        format.html { render :new }
        format.json { render json: @sensor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sensors/1
  # PATCH/PUT /sensors/1.json
  def update
    respond_to do |format|
      if @sensor.update(sensor_params)
        format.html { redirect_to :back, notice: 'Capteur mis à jour avec succès.' }
        format.json { render :show, status: :ok, location: @sensor }
      else
        format.html { render :edit }
        format.json { render json: @sensor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sensors/1
  # DELETE /sensors/1.json
  def destroy
    @sensor.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Capteur supprimé avec succès.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_sensor
    @sensor = Sensor.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def sensor_params
    params.require(:sensor).permit(:name, :sensor_type_id, :arduino_id, :serial)
  end
end