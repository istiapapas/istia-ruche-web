# encoding: utf-8
class ApplicationController < ActionController::Base

  # current_user is now accessible in both controllers and views.
  helper_method :current_user

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery
  skip_before_action :verify_authenticity_token, if: :json_request?

  before_filter :set_locale

  def error_action
    flash[:alert] = 'Vous n\'avez pas l\'autorisations pour effectuer cette action'
    redirect_to(root_path)
  end

  rescue_from CanCan::AccessDenied do |exception|
    error_action
    #redirect_to root_url, :alert => exception.message
  end

  before_filter do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  def set_locale
    I18n.locale = :fr
  end

  protected

  def json_request?
    request.format.json?
  end
end
