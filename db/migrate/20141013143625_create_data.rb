class CreateData < ActiveRecord::Migration
  def change
    create_table :data do |t|
      t.string :value
      t.integer :sensor_id
      t.timestamps
    end
  end
end
