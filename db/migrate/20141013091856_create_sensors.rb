class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.string :name
      t.integer :sensor_type_id
      t.integer :serial
      t.integer :arduino_id

      t.timestamps
    end
  end
end
