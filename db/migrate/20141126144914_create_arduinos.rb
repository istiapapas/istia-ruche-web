class CreateArduinos < ActiveRecord::Migration
  def change
    create_table :arduinos do |t|
      t.string :name
      t.integer :hive_id
      t.integer :serial

      t.timestamps
    end
  end
end
